"""Backup Tree comparer

Usage:
  main.py <orig> <back> [--maxload=<ml>] [--maxmem=<mm>] [--hit=<p>]
  main.py (-h | --help)
  main.py --version

Compares directory <orig> with directory <back> recursivly.
The program is throttled to only hit the file system up to $maxload.
For very large files only chunks of size $maxmem are loaded iteratively.

Maxload and hit probability are similar yet serve differnt purposes:
- Maxload is to slow down the program so the file system can also do other
  tasks.
- Hit is used to reduce the probability of actually comparing a file/dir and
  instead only comparing a subset of the file trees. Thus allowing the program
  to finish quicker and in reasonable time for very large file trees.
  (For instance comparing 10TB of file might not finish for weeks)

If files are equal, nothing will be output to stdout. All unequal files will be
output to stderr.

Options:
  -h --help      Show this screen.
  --version      Show version.
  --maxload=<ml> Maximum file system load to tolerate [default: 10]
  --maxmem=<mm>  Maximum memory to use when reading big files [default: 100M]
  --hit=<p>      Probability between 0 and 1 that the file or directory will be
                 hit. If 1 every file is checked if 0.1 only every 10th file.
                 [default: 0.2]

"""
################################################################################
# TODO: How to get the load? Solaris? Linux? iostat (%b field)? nfs or local
# disk?
# TODO: progress to where? When finished what to notify?
# TODO: Tree should be walked breath first? Since we don't want to just spend
# all time on one single dir?
################################################################################

from pyhashxx import hashxx # Fast hashing
from docopt import docopt # Command line option parsing
from subprocess import call # For running shell commands
import random

################################################################################
# Some evil (but simple) global variables:
curload = 1
maxload = 1
hitp = 1
sleepReq = 5 # Requested ms to sleep

################################################################################
if __name__ == '__main__':
    global hitp, maxload, maxmem
    arguments = docopt(__doc__, version='BkpTreeCmp 0.0')
    # Only gets here if both dirs were supplied
    print(arguments)
    dirl = arguments['<orig>']
    dirr = arguments['<back>']
    maxload = arguments['--maxload']
    maxmem = arguments['--maxmem']
    hitp = arguments['--hit']
    cmpDirs(dirl, dirr)

################################################################################
def cmpDirs(da, db, depth = 0):
  # Must pause thread (ie sleep) between iterations.
  # Also skips files probabilistically (@see shouldHit).
  # Should not skip anything at depth 0..2??
  return false

################################################################################
def cmpFile(fa, fb):
  # Check filesize of both files
  # If differ: Return false
  # If same see if they're larger than maxmem
  # Read in files (chuckwise or whole) and put in hash function
  # Return true if hashes are the same, otherwise false
  # Must pause thread (ie sleep) between chunks. No sleep for small files
  return false

################################################################################
def shouldHit():
  return random.uniform(0,1) < hitp

################################################################################
def poolForDir(d):
  # Given a directory return the pool (or device?)
  # Use something like
  # call(["df", "."])
  return "foo"

################################################################################
def loadOnPool(fs):
  # Given a pool return the current load between 0..1
  # Use something like
  # call(["iostat", 5]) ??
  # call(["zpool", "iostat"]) ??
  # or: fsstat?
  return 0.7

################################################################################
def adjustSleep():
  # Runs forever and keeps adjusting the sleep length. Checks load periodically
  # Also sleeps in between iterations for ~1-10sec?
  return



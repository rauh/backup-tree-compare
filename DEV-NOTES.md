# Random Notes:
- Don't compare file contents:
  - File systems might be different IO speeds and syncing would slow things down
  - Only compare hashes


# Links:
http://moinakg.wordpress.com/2013/07/01/parallel-directory-tree-compare-in-python/
https://github.com/docopt/docopt
https://code.google.com/p/xxhash/


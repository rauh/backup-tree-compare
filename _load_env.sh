# Source this into your shell:
# $ source _load_env.sh

# Initialize a virtualenv here:
virtualenv .
# Activate it
source bin/activate
# Install dependencies
pip install pyhashxx docopt

